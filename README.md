
  

# AdviceSlips backend API

This is an API for the website [Advicebits](https://advicebits.com) which handles user signups, authentication, sms sending, and contact messages.

  
## Getting Started

 
*Please make sure that you have `node` and `npm` installed.*

1. Clone this repository.
2. Navigate to the root of this directory and install dependencies: `npm install`
3. Create an `.env` file and copy the contents from the `sampleEnv.txt` and fill out the missing values.
4. Run the api: `npm start`
  
  
## Endpoints

The base url used for all endpoints is `api/` and will build off of this.

  
### Users: `/users` 
These endpoints interact with creating, authenticating, and deleting users.

#### Create a user: 
- Method: `POST`
- Endpoint: `/create`

Headers:

- `'Content-Type': 'application/json'` **required**

Body:
-  `name`  - *[type: string]*  **required**
-  `phoneNumber` -  *[type: number]* **required**

Successful Response:
- Status: `201`
- Body: `{}`

Error Response:
`{ error: 'Could not create new user', reason: '*depends on error*'}`

#### Authenticate a user: 
- Method: `GET`
- Endpoint:   `/auth/:phoneNumber/:authCode`

URL parameters:
-  `phoneNumber`  - *[type: number]*  **required**
-  `authCode` -  *[type: number, length: 6 chars]* **required**

Successful Response:
- Status: `200`
- Body: `{}`

Error Response:
`{ error: 'Could not authenticate user', reason: '*depends on error*'}`


#### Request a new authCode: 
- Method: `GET`  
- Endpoint: `/auth/:phoneNumber/new`

URL parameters:
-  `phoneNumber`  - *[type: number]*  **required**

Successful Response:
- Status: `201`
- Body: `{}`

Error Response:
`{ error: 'Could not create new authCode', reason: '*depends on error*'}`


#### Delete a user: 
- Method:`DELETE` 
- Endpoint: `/:phoneNumber`

URL parameters:
-  `phoneNumber`  - *[type: number]*  **required**

Successful Response:
- Status: `204`
- Body: `{}`

Error Response:
`{ error: 'Could not delete user', reason: '*depends on error*'}`


### Messages: `/messages` 
These endpoints interact with creating messages for us.

#### Create a message: 
- Method: `POST`
- Endpoint:  `/create`

Headers:
- `'Content-Type': 'application/json'` **required**

Body:
-  `email`  - *[type: string]*  **required**
-  `message` -  *[type: string]* **required**

Successful Response:
- Status: `201`
- Body: `{}`

Error Response:
`{ error: 'Could not create new message', reason: '*depends on error*'}`