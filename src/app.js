import config from './config';
import express from 'express';
import { createLogger, format, transports } from 'winston';
import mongoConnection from './loaders/mongo';

async function startServer() {
    const app = express();

    // Winston logger
    const logger = createLogger({
        level: config.logLevel,
        format: format.combine(
            format.colorize(),
            format.simple(),
            format.timestamp({
                format: 'MM-DD-YYYY HH:mm:ss',
            }),
            format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
        ),
        transports: [
            new transports.Console()
        ],
    });
    app.locals.logger = logger;

    // Connect to database
    const databaseConnection = await mongoConnection.createConnection();
    app.locals.db = databaseConnection;

    // Add defaults to http server
    await require('./loaders').default({ expressApp: app });

    // Tell http server to list to port
    app.listen(config.port, err => {
        if (err) {
            console.error(err);
            process.exit(1);
            return;
        }

        logger.info(`Environment: ${config.runningEnv}`)
        logger.info(`Server listening on port: ${config.port}`);
    });
}

startServer();