// Middleware for checking your requests
class HttpMiddleware {

    // Check post request
    async basicGet(req, res, next) {

        // Bad method
        if (req.method !== 'GET') {
            return res.status(400).json({
                error: 'Bad GET request',
                reason: 'Invalid request type for this route, must be GET'
            });
        }

        // Looks good
        return next();
    }

    // Check post request
    async basicPost(req, res, next) {

        // Bad method
        if (req.method !== 'POST') {
            return res.status(400).json({
                error: 'Bad POST request',
                reason: 'Invalid request type for this route, must be POST'
            });
        }

        const headers = req.headers;

        // Missing Content-Type header
        if (!('content-type' in headers)) {
            return res.status(400).json({
                error: 'Bad POST request',
                reason: 'Missing Content-Type header'
            });
        }
        
        // Content-Type must be application/json
        if (headers['content-type'] !== 'application/json') {
            return res.status(400).json({
                error: 'Bad POST request',
                reason: 'Invalid Content-Type header, must be application/json'
            });
        }

        // Missing body
        if (!req.body) {
            return res.status(400).json({
                error: 'Bad POST request',
                reason: 'Missing JSON body'
            });
        }

        // Looks good
        return next();
    };

    // Simple checks for DELETE
    async basicDelete(req, res, next) {
        // Bad method
        if (req.method !== 'DELETE') {
            return res.status(400).json({
                error: 'Bad DELETE request',
                reason: 'Invalid request type for this route, must be DELETE'
            });
        };

        // Looks good
        return next();
    }
}

module.exports = new HttpMiddleware();