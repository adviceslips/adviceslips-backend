import { Router, Request, Response } from 'express';
import MessageService from '../../services/messages';

// Middleware
import HttpMiddleware from '../middlewares/http';

const route = Router();

// All contact passes through here
export default (app) => {
    // Route appended to ex. /api/messages
    app.use('/messages', route);

    // Creates a new user
    route.post('/create', HttpMiddleware.basicPost, async (req, res) => {        
        const logger = req.app.locals.logger;

        // Missing email parameter
        if (!('email' in req.body)) {
            return res.status(400).json({
                error: 'Could not add new message',
                reason: 'Missing email parameter'
            })
        }

        // Missing phoneNumber parameter
        if (!('message' in req.body)) {
            return res.status(400).json({
                error: 'Could not add new user',
                reason: 'Missing message parameter'
            });
        }

        logger.info(`Adding a new message`);

        // Get database connection
        const db = req.app.locals.db;
        const email = req.body.email;
        const message = req.body.message;
        
        try {
            // Add user to database
            await MessageService.addMessage(email, message, { dbConnection: db, logger });
            return res.json().status(201);
        } catch(err) {
            logger.error(err);

            return res.status(500).json({
                error: 'Could not add new message',
                reason: 'There was an error while adding message to database'
            });
        };
    });
};