import { Router, Request, Response } from 'express';
import UserService from '../../services/users';
import AuthService from '../../services/auth';
import SmsService from '../../services/sms';

// Middleware
import HttpMiddleware from '../middlewares/http';

const route = Router();

// All store information passes through here
export default (app) => {
    // Route appended to ex. /api/users
    app.use('/users', route);

    // Creates a new user
    route.post('/create', HttpMiddleware.basicPost, async (req, res) => {        
        const logger = req.app.locals.logger;

        // Missing name parameter
        if (!('name' in req.body)) {
            return res.status(400).json({
                error: 'Could not add new user',
                reason: 'Missing name parameter'
            })
        }

        // Missing phoneNumber parameter
        if (!('phoneNumber' in req.body)) {
            return res.status(400).json({
                error: 'Could not add new user',
                reason: 'Missing phoneNumber parameter'
            });
        }

        // Get database connection
        const db = req.app.locals.db;
        const name = req.body.name;
        const phoneNumber = req.body.phoneNumber;
        
        try {
            // Add user to database
            logger.info(`Adding a new user: ${phoneNumber}`);            
            await UserService.addUser(name, phoneNumber, { dbConnection: db, logger });
            logger.info(`User created: ${phoneNumber}`);

            // Create auth code
            logger.info(`Creating auth code: ${phoneNumber}`);
            const authCode = await AuthService.createAuth(phoneNumber, { dbConnection: db, logger });
            logger.info(`Auth code created: ${phoneNumber}`);

            // Send sms
            logger.info(`Sending signup/2FA sms: ${phoneNumber}`);
            const message = `Thanks for joining AdviceBits!\n\nAuth code: ${authCode}\nThis code will expire in 15 minutes\n\nIf you did not sign up for our service please ignore this message and you will not receive our daily texts`;
            await SmsService.sendSms(phoneNumber, message, { dbConnection: db, logger });
            logger.info(`Sent 2FA sms: ${phoneNumber}`);

            // Send back success
            return res.status(201).json();
        } catch(err) {
            logger.error(err);

            // We passed a nice error message
            if (err.status && err.message) {
                return res.status(err.status).json({
                    error: 'Could not create new user',
                    reason: err.message,
                });
            };

            // Generic error, try to avoid
            return res.status(500).json({
                error: 'Could not create new user',
                reason: 'There was an error while adding to the database'
            });
        };
    });

    // Send new auth code to user
    route.get('/auth/:phoneNumber/new', HttpMiddleware.basicGet, async (req, res) => {        
        const logger = req.app.locals.logger;

        // Url vars
        const phoneNumber = req.params.phoneNumber;

        // Not number
        if (!parseInt(phoneNumber)) {
            return res.status(400).json({
                error: 'Could not delete user',
                reason: 'Invalid parameter type: phoneNumber, must be number'
            });
        };

        // Get database connection
        const db = req.app.locals.db;

        try {
            // Add user to database
            logger.info(`Creating new authCode: ${phoneNumber}`);            
            const authCode = await AuthService.createAuth(phoneNumber, { dbConnection: db, logger });
            logger.info(`Created new authCode: ${phoneNumber}`);

            // Send sms
            logger.info(`Sending new 2FA authCode sms: ${phoneNumber}`);
            const message = `AdviceBits\n\nAuth code: ${authCode}\nThis code will expire in 15 minutes\n\nIf you did not sign up for our service please ignore this message and you will not receive our daily texts`;
            await SmsService.sendSms(phoneNumber, message, { dbConnection: db, logger });
            logger.info(`Sent new 2FA authCode sms: ${phoneNumber}`);

            // Send back success
            return res.status(201).json();
        } catch(err) {
            // We passed a nice error message
            if (err.status && err.message) {
                return res.status(err.status).json({
                    error: 'Could not create new authCode',
                    reason: err.message,
                });
            };

            // Generic error, try to avoid
            return res.status(500).json({
                error: 'Could not create new authCode',
                reason: 'There was an error while contacting with the database'
            });
        };
    });

    // Authenticate a new user with 2FA
    route.get('/auth/:phoneNumber/:authCode', HttpMiddleware.basicGet, async (req, res) => {        
        const logger = req.app.locals.logger;

        // Url vars
        const phoneNumber = req.params.phoneNumber;
        const authCode = req.params.authCode;

        // Not number
        if (!parseInt(phoneNumber)) {
            return res.status(400).json({
                error: 'Could not delete user',
                reason: 'Invalid parameter type: phoneNumber, must be number'
            });
        }

        // Not number
        if (!parseInt(authCode)) {
            return res.status(400).json({
                error: 'Could not authenticate user',
                reason: 'Invalid parameter type: authCode, must be number'
            });
        } else if (authCode.length !== 6) {
            return res.status(400).json({
                error: 'Could authenticate user',
                reason: 'Invalid parameter length: authCode, must be 6 characters'
            });
        }

        // Get database connection
        const db = req.app.locals.db;

        try {
            // Add user to database
            logger.info(`Authenticating user: ${phoneNumber}`);            
            await AuthService.authUser(phoneNumber, authCode, { dbConnection: db, logger });
            
            // Send back success
            logger.info(`User authenticated: ${phoneNumber}`);
            return res.json();
        } catch(err) {
            // We passed a nice error message
            if (err.status && err.message) {
                return res.status(err.status).json({
                    error: 'Could not authenticate user',
                    reason: err.message,
                });
            };

            // Generic error, try to avoid
            return res.status(500).json({
                error: 'Could not authenticate user',
                reason: 'There was an error while checking with the database'
            });
        };
    });

    // Deletes a user from contacts
    route.delete('/:phoneNumber', HttpMiddleware.basicDelete, async (req, res) => {        
        const logger = req.app.locals.logger;
        const phoneNumber = req.params.phoneNumber;

        // Not number
        if (!parseInt(phoneNumber)) {
            return res.status(400).json({
                error: 'Could not delete user',
                reason: 'Invalid parameter type: phoneNumber, must be number'
            });
        };

        // Length check
        // if (phoneNumber.length !== 10) {
        //     return res.status(400).json({
        //         error: 'Could not delete user',
        //         reason: 'Parameter phoneNumber must be 10 in length'
        //     });
        // }


        // Get database connection
        const db = req.app.locals.db;
        
        try {
            // Add user to database
            logger.info(`Deleting user: ${phoneNumber}`);
            await UserService.deleteUser(phoneNumber, { dbConnection: db, logger });
            
            // Send back success
            logger.info(`Deleted user: ${phoneNumber}`);
            return res.status(204).json();
        } catch(err) {
            // We passed a nice error message
            if (err.status && err.message) {
                return res.status(err.status).json({
                    error: 'Could not delete user',
                    reason: err.message,
                });
            };

            // Generic error, try to avoid
            return res.status(500).json({
                error: 'Could not delete user',
                reason: 'There was an error while deleting from database'
            });
        };
    });
};