import { Router } from 'express';
import users from './routes/users';
import messages from './routes/messages';

export default () => {
    const app = Router();
    
    // Additional routes
    users(app);
    messages(app);

	return app
}