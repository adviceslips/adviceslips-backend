import moment from 'moment';

// Handles any interactions with users
class MessageService {

    // Add a new user with their info
    async addMessage(email, message, { dbConnection, logger }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        }

        if (!logger) Error('Missing logger');

        try {
            logger.debug('Querying: inserting new message');

            // Insert message
            const collection = dbConnection.collection('messages');
            const results = await collection.insertOne({
                email,
                message,
                date: moment().toISOString(),
            });

            logger.debug('Query done: inserted new message');

            // Send back success
            return results;
        } catch (err) {
            return err;
        };
    };
};

module.exports = new MessageService();