import moment from 'moment'; // Time

// Handles any interactions with users
class UserService {

    // Add a new user with their info
    async addUser(name, phoneNumber, { dbConnection, logger }) {        
        // Missing dbConnection or logger
        if (!dbConnection) throw Error('Missing database connection');
        if (!logger) throw Error('Missing logger');

        try {
            const contactsCollection = dbConnection.collection('contacts');
            const foundUser = await contactsCollection.find({ number: phoneNumber }).toArray();

            // User already exists
            if (foundUser.length > 0) {
                throw {
                    status: 500,
                    message: 'user already exists',
                };
            };

            logger.debug(`Inserting new user: ${phoneNumber}`);

            // Insert if not exists, check by phoneNumber
            const saveUser = await contactsCollection.insertOne({
                name,
                number: phoneNumber,
                isAuthed: false,
                timeSaved: moment().toISOString(),
            });

            // Failed to save user
            if (saveUser.result.insertedCount === 0) {
                logger.error(saveUser);

                throw {
                    status: 500,
                    message: 'Failed to save user to database',
                };
            }

            // Send back success
            logger.debug(`User saved to database: ${phoneNumber}`);
            return 'success';
        } catch (err) {
            // We threw this error
            if (err.message && err.status) {
                throw err
            } else {
                // Generic error
                throw { status: 500, message: 'Internal server error while adding user' }
            }
        };
    };

    // Remove user from list to send texts to
    async deleteUser(phoneNumber, { dbConnection, logger }) {
        // Missing dbConnection or logger
        if (!dbConnection) throw Error('Missing database connection');
        if (!logger) throw Error('Missing logger');

        try {
            // Delete user using phone number from contacts collection
            const collection = dbConnection.collection('contacts');
            const deletedResult = await collection.deleteOne({ number: phoneNumber });
            
            // Didn't find user to delete, may not exist
            if (deletedResult.deletedCount === 0) return logger.warn('Deleted 0 users, may not exist');

            logger.debug(`User removed: ${phoneNumber}`);

            // Send back success
            return;
        } catch (err) {
            // We threw this error
            if (err.message && err.status) {
                logger.warn(err);
                throw err
            } else {
                logger.err(err);
                // Generic error
                throw { status: 500, message: 'Internal server error while authenticating user' }
            }
        };          
    };
};

module.exports = new UserService();