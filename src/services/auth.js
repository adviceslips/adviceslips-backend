import moment from 'moment';

// Handles any interactions with sms
class AuthService {

    // Create new auth code & save to auth collection
    async createAuth(phoneNumber, { dbConnection, logger }) {
        // Missing dbConnection or logger
        if (!dbConnection) throw Error('Missing database connection');
        if (!logger) throw Error('Missing logger');

        try {
            logger.debug(`Creating auth record (2FA): ${phoneNumber}`)

            // Create authCode & expire time
            const newAuthCode = Math.floor(100000 + Math.random() * 900000);
            const validTill = moment().add('15', 'minutes').unix()

            // Save to auth collection
            const authCollection = dbConnection.collection('auth');
            const authSaved = await authCollection.insertOne({
                number: phoneNumber,
                authCode: newAuthCode,
                validTill
            });

            // Failed to insert new authCode for user
            if (authSaved.result.insertedCount === 0) {
                logger.error(authSaved);

                throw {
                    status: 500,
                    message: 'Failed to give user authCode',
                };
            }

            logger.debug(`Authentication record created: ${phoneNumber}`)

            // Send back newly created auth code
            return newAuthCode;
        } catch (err) {
            // We threw this error
            if (err.message && err.status) {
                logger.warn(err);
                throw err
            } else {
                logger.error(err);
                // Generic error
                throw { status: 500, message: 'Internal server error while authenticating user' }
            }
        };          
    };

    // Authenticate user so they can begin getting texts
    async authUser(phoneNumber, authCode, { dbConnection, logger }) {
        // Missing dbConnection or logger
        if (!dbConnection) throw Error('Missing database connection');
        if (!logger) throw Error('Missing logger');

        try {
            logger.debug(`Finding user in auth collection: ${phoneNumber}`);

            // Get user's valid authCode(s) from db
            const authCollection = dbConnection.collection('auth');
            const foundAuthCodes = await authCollection.find({ number: phoneNumber }).toArray();
            
            // Didn't find any authCodes for user
            if (foundAuthCodes.length === 0) {
                logger.debug(`No auth record(s) found: ${phoneNumber}`);

                throw {
                    status: 401,
                    message: 'User does not have any valid authCodes',
                };
            };

            logger.debug(`Validating authCodes: ${phoneNumber}`);

            // Validate submitted authCode with found codes for user
            let hasValidAuth = false;
            foundAuthCodes.map(authCodeInfo => {
                // Matches submitted code
                if (parseInt(authCodeInfo.authCode) === parseInt(authCode)) {
                    // Expired
                    if (authCodeInfo.validTill < moment().unix()) {
                        // Delete this auth code
                        authCollection.deleteOne({ authCode: authCodeInfo.authCode });
                        throw { status: 401, message: 'authCode has expired' };
                    }
                    
                    hasValidAuth = true;
                };
            });

            // User's authCode did not match any saved authCodes
            if (!hasValidAuth) {
                throw {
                    status: 401,
                    message: 'provided authCode is invalid',
                };
            }

            logger.debug(`Deleting old authCodes: ${phoneNumber}`)

            // Delete any authCodes with user phoneNumber
            await authCollection.deleteMany({ number: phoneNumber });

            logger.debug(`Updating auth permissions: ${phoneNumber}`)
            
            // Update user's auth status
            const contactsCollection = dbConnection.collection('contacts');
            const updateResults = await contactsCollection.updateOne(
                { number: phoneNumber },
                { $set: { isAuthed: true } },
            );

            // Failed to update user
            if (updateResults.result.nModified === 0) {
                throw {
                    status: 500,
                    message: 'Failed to update isAuthed on user',
                };
            };

            // Send back success
            return 'success';
        } catch (err) {
            // We threw this error
            if (err.message && err.status) {
                logger.warn(err.message);
                throw err
            } else {
                logger.err(err);
                // Generic error
                throw { status: 500, message: 'Internal server error while authenticating user' }
            }
        };          
    };
};

module.exports = new AuthService();