import Twilio from 'twilio'; // SMS
import config from '../config';

// Handles any interactions with sms
class SmsService {

    // Remove user from list to send texts to
    async sendSms(phoneNumber, message, { dbConnection, logger }) {
        // Missing dbConnection or logger
        if (!dbConnection) throw Error('Missing database connection');
        if (!logger) throw Error('Missing logger');

        try {
            logger.debug('Connecting to twilio');
            const client = Twilio(config.twilioSID, config.twilioAuthToken);
            
            // Send sms text
            logger.debug(`Sending sms: ${phoneNumber}`);
            await client.messages.create({
                to: phoneNumber,
                from: config.twilioPhoneNumber,
                body: message,
            });

            logger.debug(`Sms sent: ${phoneNumber}`);

            // Send back success
            return;
        } catch (err) {
            console.log(err)
            // We threw this error
            if (err.message && err.status) {
                throw err;
            } else {
                logger.error(err);
                // Generic error
                throw { status: 500, message: 'Internal server error while authenticating user' };
            };
        };          
    };
};

module.exports = new SmsService();