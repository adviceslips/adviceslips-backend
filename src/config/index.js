import dotenv from 'dotenv';

const validEnvs = ['development', 'testing', 'production']
const envFound = dotenv.config();
if (envFound.error) {
    // This error should crash whole process
    throw new Error("Couldn't find .env file");
};

// Validate user chose a supported environment
if (!validEnvs.includes(process.env.NODE_ENV)) {
    throw new Error('Invalid environment selected, must be: development, testing, or production');
}

export default {
    // Express
    runningEnv: process.env.NODE_ENV, 
    port: parseInt(process.env.EXPRESS_PORT, 10),
    rateLimit: process.env.REQUESTS_RATE_LIMIT,
    logLevel: process.env.LOG_LEVEL,

    // Database
    databaseURL: process.env.MONGODB_URI,
    databaseName: process.env.MONGODB_DATABASE,

    // Twilio
    twilioSID: process.env.NODE_ENV === 'development' ? process.env.TWILIO_TEST_SID : process.env.TWILIO_SID,
    twilioAuthToken: process.env.NODE_ENV === 'development' ? process.env.TWILIO_TEST_AUTH_TOKEN : process.env.TWILIO_AUTH_TOKEN,
    twilioPhoneNumber: process.env.NODE_ENV === 'development' ? process.env.TWILIO_TEST_NUMBER : process.env.TWILIO_NUMBER,

    //API configs
    api: {
        prefix: '/api',
    },
};