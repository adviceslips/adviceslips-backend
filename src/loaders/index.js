import expressLoader from './express';
import config from '../config';
import { createLogger, format, transports } from 'winston';

// Winston logger
const logger = createLogger({
    level: config.logLevel,
    format: format.combine(format.colorize(), format.simple()),
    transports: [
        new transports.Console()
    ],
});

export default async ({ expressApp }) => {
    // Setup express stuff
    expressLoader({ app: expressApp });
    logger.info('Express loaded');
};