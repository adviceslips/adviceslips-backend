import mongodb from 'mongodb';
import config from '../config';
import { createLogger, format, transports } from 'winston';

// Winston logger
const logger = createLogger({
    level: config.logLevel,
    format: format.combine(format.colorize(), format.simple()),
    transports: [
        new transports.Console()
    ],
});

// Used for creating connection to mongo database
class MongoConnection {
    constructor() {
        // Connection client
        const url = config.databaseURL;
        this.client = mongodb.MongoClient(url, { useUnifiedTopology: true });
    }

    // Create connection
    async createConnection() {
        logger.info('Connecting to database');

        // Connect to mongo
        await this.client.connect();
        logger.info('Connected');

        const db = this.client.db(config.databaseName);
        return db;
    }

    // Kill connection
    async closeConnection() {
        logger.info('Closing connection to database');
        this.client.close();
    }
};

module.exports = new MongoConnection();